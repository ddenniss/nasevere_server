<!DOCTYPE HTML>
<!-- Make NaSevere Great Again! -->
<?php include 'blocks.php' ?>
<html>
<?php $PAGE_ID = "contacts" ?>
<?= page_header($PAGE_ID) ?>

<body>

<header>

	<div class="container fixed-width">
		<div class="row">
			<div class="col-sm-12">
				<img src="/img/header.png" alt="header.png" width="750" height="100" class="ccm-image-block img-responsive bID-90">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<?= page_navbar($PAGE_ID) ?>
			</div>
		</div>
	</div>
</header>

<div class="container fixed-width" style="margin-top: 20px">
	<div class="row">
		<div class="col-sm-12">

			<h1 class="page-title">Наши контакты</h1>



	    <p>Если вы решили разместить рекламу в нашей группе, напишите одному из рекламных модераторов:</p>

			<ul style="list-style-type:square">
				<li><a href="https://vk.com/desantnik3">Александр Долженко</a></li>
				<li><a href="https://vk.com/id192604448">Елена Ушакова</a></li>
				<li><a href="https://vk.com/roxolana77">Ксения Филиппова</a></li>
			</ul>

			<p>Задавайте им вопросы, если что-то не ясно, не понятно. Они с удовольствиям вам ответят и помогут!  </p>
		</div>
	</div>
</div>

<?= footer() ?>

<?= yandex_counter() ?>

</body>
</html>
