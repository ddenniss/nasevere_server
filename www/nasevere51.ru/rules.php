<!DOCTYPE HTML>
<!-- Make NaSevere Great Again! -->
<?php include 'blocks.php' ?>
<html>
<?php $PAGE_ID = "rules" ?>
<?= page_header($PAGE_ID) ?>

<body>

<header>

	<div class="container fixed-width">
		<div class="row">
			<div class="col-sm-12">
				<img src="/img/header.png" alt="header.png" width="750" height="100" class="ccm-image-block img-responsive bID-90">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<?= page_navbar($PAGE_ID) ?>
			</div>
		</div>
	</div>
</header>

<div class="container fixed-width" style="margin-top: 20px">
	<div class="row">
		<div class="col-sm-12">


    <h1 class="page-title">Правила группы</h1>

		<h2>0. Термины</h2>

		<c><n>0.1</n> <d>Пост</d> – запись от имени группы или участника группы на стене группы или в темах.</c>
		<c><n>0.2</n> <d>Закреп</d>, <d>закреплённый пост</d> – пост на стене группы, использующий функцию вк «закрепление». В зависимости от устройства и приложения просмотра ВКонтакте, располагается наверху группы сразу под названием и статусом, либо является самым первым постом на стене на всё время закрепления.</c>
		<c><n>0.3</n> <d>Реклама</d>, <d>рекламный пост</d> – объявление, оповещающее о товаре или услуге с целью их регулярной продажи и получения коммерческой выгоды, либо компенсации расходов.</c>
		<c><n>0.4</n> <d>Бан</d> – блокировка участника в группы, в результате которой тот лишается доступа в группу. Производится путём добавления в "чёрный список".</c>
		<c><n>0.5</n> <d>Лайк</d> – "Мне нравится". Лайковый – собравший лайки. Самый лайковый – собравший наибольшее количество лайков.</c>
		<c><n>0.6</n> <d>Несрочная реклама</d> – реклама, не содержащая привязки ко времени, не устанавливающая крайние сроки. Например, конкурс репостов или информация о праздничной акции в магазине – это срочная реклама, а реклама магазина бытовой техники или услуги портретов на заказ – несрочная.</c>
		<c><n>0.7</n> <d>Оффтоп(ик)</d> - текст в комментариях или постах, по смыслу не соответствующий комментируемому посту или заданной теме.</c>
		<c><n>0.8</n> <d>Поднятие объявления</d> - удаление старого объявления и пост нового с аналогичным или похожим содержанием.</c>

		<h2>1. Общие замечания и требования.</h2>

		<c><n>1.1</n> Главная цель группы – быть всё более полной и полезной онлайн доской объявлений для Печенгского района.</c>
		<c><n>1.2</n> Посты явно или неявно должны адресоваться к жителям Печенгского района. Если товар находится вне пределов района, должны быть предусмотрены работающие способы доставки и оплаты, информацию о которых необходимо включить в текст поста.</c>
		<c><n>1.3</n> Администрация группы может на свое усмотрение отказывать в публикации, либо удалять уже опубликованные объявления, которые она сочтёт нежелательными. Администрация оставляет за собой право не объяснять причины удаления.</c>
		<c><n>1.4</n> Посты публикуются на русском языке, либо с прилагающимся переводом на русский.</c>
		<c><n>1.5</n> <b>Вежливость.</b> Мат, грубость, неуважительное отношение к другим участникам в группе – недопустимо. Нарушители подвергаются санкциям.</c>
		<c><n>1.6</n> Наша группа с ноября 2014 года является закрытой. Для вступления в группу нужно подать заявку. Рекомендуем иметь заполненные данные в вашем аккаунте, показывающие, что вы не спамер и имеете отношение к Печенгскому району, иначе ваша заявка может быть отклонена.</c>
		<c><n>1.7</n> В нашей группе разрешаются дублирующие аккаунты под не настоящим именем, т.к. мы понимаем, что иногда так удобнее пользователям – иметь возможность задавать вопросы и размещать объявления, не раскрывая свою личность. Возможно иметь не более одного дополнительного аккаунта на человека.</c>
				<cc><n>1.7.1</n> Исключение из этого правила - случай, когда другой аккаунт того же пользователя находится в черном списке. До момента окончания срока бана, дублирующие аккаунты приниматься не будут.</cc>

		<h2>2. Частота размещения постов.</h2>
		<intro>Для того, чтобы стена группы была как можно более разнообразной и полезной, вводятся ограничения на частоту публикации постов на стене.</intro>

		<c><n>2.1</n> Каждый день (каждое календарное число) можно размещать по два разных объявления.</c>
				<cc><n>2.1.1</n> Увеличение частоты размещения постов с дублирующих аккаунтов или через аккаунты друзей запрещено. При установлении такого случая, в чёрный список могут быть добавлены все участвующие в размещении объявления аккаунты.</cc>
				<cc><n>2.1.2</n> <strike>Удаление своего объявления с целью его размещения вновь, или размещения вместо него другого объявления (если прошло менее 2 дней) запрещено.</strike></cc>
				<cc><n>2.1.3</n> При подсчете частотности не учитываются посты такого же содержания, но от имени группы.</cc>
				<cc><n>2.1.4</n> <strike>Для постов от имени группы минимальный допустимый интервал между одинаковыми постами - 24 часа</strike></cc>
				<cc><n>2.1.5</n> Удаление старых, даже не актуальных более постов, запрещено</cc>
		<c><n>2.2</n> Разрешены объявления о продаже товаров, изготовленных собственными руками (рукоделие). В таких товарах должен присутствовать элемент творчества и оригинального исполнения.</c>
				<cc><n>2.2.1</n> Минимальный интервал между объявлениями от одного автора работ, или его представителя в группе – <b>4 суток (96 часов).</b> Дублирование объявления другими пользователями запрещено.</cc>
				<cc><n>2.2.2</n> Каждое последующее объявление должно по форме или содержанию отличаться от предыдущего.</cc>
				<cc><n>2.2.3</n> Разрешаются объявления с предложением изготовить подобный товар на заказ.</cc>
				<cc><n>2.2.4</n> В категорию изготовленных собственноручно не входят следующие виды товаров с малой долей вложенного в них ручного творчества: кондитерские и любые съедобные изделия; сувенирная и прочая продукция, мелкосерийно изготовленная с помощью специальных устройств (например кружки, магниты, футболки с принтом).</cc>
				<cc><n>2.2.5</n> В категорию рукодельных товаров не входят пошитые медицинские маски.</cc>
						<ccc><strike><n>2.2.5.1</n> На период карантина в РФ ВКонтакте запретило рекламу медицинских масок на продажу. Соответственно мы также не можем дать пост как рекламу от имени группы, если в посте предлагается купить маски.</strike></ccc>
		<c><n>2.3</n> <b>С 25 июля 2016 года,</b> объявления о продаже товаров привезенных из Норвегии и Финляндии могут публиковаться только от имени группы (как реклама).</c>

		<h2>3. Содержание постов.</h2>
		<intro>Мы вводим ограничения на содержание публикуемых текстов и изображений.</intro>

		<c><n>3.1</n> В группе запрещены посты связанные с продажей, покупкой, демонстрацией оружия, наркотиков, порнографической продукции (включая порнографические изображения), а также других товаров и услуг, запрещенных к рекламе и продаже законодательством РФ и правилами VK.</c>
				<cc><n>3.1.1</n> К категории оружия в нашей модераторской трактовке относится также пневматическое оружие и поэтому объявления, связанные с его покупкой или продажей на стене запрещены. Реклама оружия, если она не противоречит законам РФ, возможна в специальной теме.</cc>
				<cc><n>3.1.2</n> Список запрещенных к публикации (предложения о покупке, продаже) тем:
					<ul class="inner">
						<li>Оружие</li>
						<li>Наркотики</li>
						<li>Порнография</li>
						<li>Алкогольная продукция</li>
						<li>Рецептурные препараты</li>
						<li>Посты с обсценной лексикой</li>
					</ul>
				</cc>
		<c><n>3.2</n> Реклама <b>алкоголя</b>, равно как и частные объявления о его продаже запрещены во всех постах, как на стене, так и в темах и в других частях группы.</c>
		<c><n>3.3</n> Рекламные посты на стене возможны только от имени группы (раздел <b>7</b>), за исключением случаев, предусмотренных в разделе <b>2</b>. Рекламой являются и объявления о платных услугах (п. <b>0.3</b>)</c>
				<cc><n>3.3.1</n> Публикация рекламы возможна в соответствующих темах: "Реклама магазинов", "Услуги".</cc>
				<cc><n>3.3.2</n> Реклама приветствуется в ответах на вопросы участников группы, связанные с поискои товаров и услуг.</cc>
				<cc><n>3.3.3</n> Объявления об изготовлении товара, оказании услуг на заказ рассматриваются как реклама.</cc>
				<cc><n>3.3.4</n> Регулярная продажа вещей из групп <b>совместных закупок</b> рассматривается как бизнес, соответственно объявления о продаже – как реклама.</cc>
		<c><n>3.4</n> Объявления, информирующие о группах, сайтах, приложениях и т.д., либо ссылки на них, квалифицируются как реклама и имеют те же ограничения.</c>
				<cc><n>3.4.1</n> Новостные сайты составляют исключение. Но следует помнить, что новостная информация должна быть интересна участникам группы (пункт <b>3.5</b>).</cc>
		<c><n>3.5</n> <b>Спам.</b> Не относящаяся к Печенгскому району информация, набравшая менее 30 лайков может быть квалифицирована как спам с последующими санкциями в адрес автора поста.</c>
		<c><n>3.6</n> <b>Обсценная лексика.</b> Посты, содержащие мат или бранные слова, подлежат удалению и не могут быть размещены от имени группы. Смотрите пункт <b>1.5</b></c>
		<c><n>3.7</n> Криминальные новости, негативная информация о персоналиях, живущих в Печенгском районе должна даваться с указанием на источник в сми.</c>
		<c><n>3.8</n> <strike>Объявление должно содержать как минимум одно предложение текстом с указанием города. Одной картинки, даже если она содержит полное описание вставленным в нее текстом –недостаточно. Такие объявления могут быть удалены со стены.</strike></c>
				<cc><n>3.8.1</n> <strike>При продаже вещей и при предложении услуг также необходимо текстом указывать название продаваемой вещи или предлагаемой услуги.</strike></cc>
		<c><n>3.9</n> Маркетологические опросы (будет ли пользоваться спросом товар, магазин, если его открыть и т.п.) публикуются только от имени группы.</c>
		<c><n>3.10</n> Допускаются объявления о поиске подработки, если подработка не совпадает с основной профессиональной деятельностью. Такие объявления также не должны содержать цены на предлагаемые услуги.</c>
		<c><n>3.11</n> <b>Некрологи</b> следует размещать в отдельную тему <a href="https://vk.com/topic-6126497_47060102">Некрологи</a>, со стены сообщения о гибели и смерти жителей района будут удаляться - т.к. многим эмоционально тяжело в одной ленте перемежать информацию бытового и развлекательного типа с информацией о смерти.</c>
				<cc><n>3.11.1</n> Крайне желательно написать в некрологе о том, кем человек работал, что хорошего успел сделать, кто его потомки. Наверняка и сам этот человек хотел бы, чтобы напоследок широкая публика узнала о нем с хорошей стороны.</cc>
		<c><n>3.12</n> Объявления о продаже морепродуктов (семги, крабов, печени трески, морского гребешка) возможны только на рекламной основе.</c>

		<h2>4. Компоновка и дизайн объявлений.</h2>
		<intro>Участники могут компоновать свои объявления таким образом, чтобы избежать нарушений. Наша группа приветствует хороший дизайн рекламных и частных объявлений.</intro>

		<c><n>4.1</n> Несколько объявлений могут быть соединены в один пост.</c>
		<c><n>4.2</n> Объявления с синтаксическими и грамматическими ошибками допустимы – они не удаляются и не редактируются. Но постарайтесь писать грамотнее, ведь от этого будет зависеть количество откликов на ваше объявление.</c>
		<c><n>4.3</n> <strike>В посте должно быть не более 10 прикреплённых фотографий.</strike></c>
		<c><n>4.4</n> Фотографии и изображения, прикреплённые к посту, должны быть пристойного содержания.</c>
		<c><n>4.5</n> Объявление должно содержать текст. Одной картинки, даже если она содержит полное описание вставленным в нее текстом –недостаточно. Такие объявления могут быть удалены со стены.</c>
				<cc><n>4.5.1</n> При продаже вещей и при предложении услуг необходимо текстом указывать название продаваемой вещи или предлагаемой услуги. Тогда другие участники смогут найти это объявление по поиску на стене.</cc>
				<cc><n>4.5.2</n> Скриншот товара должен быть сопровожден текстовым описанием.</cc>
				<cc><n>4.5.3</n> Если в объявлении продаются вещи или предлагаются услуги, то необходимо указать город - Заполярный, Никель, Спутник и т.д.</cc>

		<h2>5. Другие ограничения.</h2>
		<intro>Обратите внимания и на другие существующие ограничения:</intro>

		<c><n>5.1</n> Запрещено подавать одно объявление с разных аккаунтов.</c>
		<c><n>5.2</n> <strike>В фотоальбомах группы возможна публикация не более трёх фотографий подряд от одного пользователя.</strike></c>
		<c><n>5.3</n> Продажа <b>подгузников</b> возможна только в соответствующей теме.</c>
		<c><n>5.4</n> Продажа <b>продукции сетевых компаний</b> (Avon, Oriflame, Faberlic, Armelle, Energy diet, Mary kay и др.) возможна только в соответствующей теме\альбоме или от имени группы</c>
		<c><n>5.5</n> За <b>оффтоп-записи</b> может последовать бан. Примеры таких записей:</c>
				<cc><n>5.5.1</n> Рекламное или частное объявление в комментариях под постом от имени группы.</cc>
				<cc><n>5.5.2</n> Рекламное или частное объявление в комментариях под чужим постом, не имеющее к этому посту смыслового отношения.</cc>
				<cc><n>5.5.3</n> Реклама в комментариях под чужой рекламой.</cc>
				<cc><n>5.5.4</n> Реклама в комментариях под объявлением о продаже товара. А также предложения аналогичного товара/услуги в комментариях.</cc>
				<cc><n>5.5.5</n> Реклама товаров или услуг в комментариях к заданному вопросу, если она явно не соответствует указанному в тексте ценовому ожиданию спрашивающего.</cc>
				<cc><n>5.5.6</n> Запрещены комментарии от имени пабликов под постами на стене, если аватар паблика заключает в себе определенную рекламную информацию и эта информация не совпадает по теме с темой поста.</cc>
		<c><n>5.6</n> <strike>Ограничение по общему количеству постов: в течение года одно и то же обьявление (по смыслу) можно публиковать не более 50 раз. Считается количество публикаций по всем пользователям.</strike></c>
		<c><n>5.7</n> Продажа <b>животных</b> возможна в соответствующей теме или от имени группы.</c>
				<cc><n>5.7.1</n> Посты о передержке <b>домашних животных</b> и прочей помощи животным разрешаются только если это оригинальная информация. Перепосты, переизложение информации с других групп или постов запрещено. Для перепоста можно воспользоваться возможностями пункта <b>8.2</b></cc>
		<c><n>5.8</n> Аватарка участника группы, при размещении поста или комментария, не должна быть рекламой магазинов, товаров или услуг. При заявке на прием в группу, такие аккаунты будут отклоняться.</c>

		<h2>6. Санкции.</h2>
		<intro>К нарушителям правил могут быть применены санкции</intro>

		<c><n>6.1</n> Виды санкций:</c>
				<cc><n>6.1.1</n> Удаление поста или комментария.</cc>
				<cc><n>6.1.2</n> Бан (см. <b>0.4</b>) на сроки различной длительности.</cc>
		<c><n>6.2</n> Санкции накладываются в прогрессирующем порядке. После первого нарушения обычно следует предупреждение от администрации группы, либо короткий бан на час или сутки, затем – на неделю и т.д.</c>
				<cc><n>6.2.1</n> Последовательность санкций не является обязательной и зависит от "тяжести" нарушения, личности участника группы и т.д. Конечное решение принимает представитель администрации группы.</cc>

		<h2>7. Реклама.</h2>
		<intro>Вы можете разместить ваш пост от имени группы, будь то реклама, поздравление или обычное частное объявление. По поводу размещения напишите одному из модераторов из списка контактов группы.</intro>

		<c><n>7.1</n> <strike>Пост одинакового содержания публикуется от имени группы не чаще одного раза в двое суток.</strike></c>
		<c><n>7.2</n> Мы не удаляем негативные отзывы о рекламируемых товарах и услугах в комментариях, если они не нарушают остальные правила группы. Этим мы хотим стимулировать публикацию действительно стоящей рекламы, полезной участникам нашей группы.</c>
		<c><n>7.3</n> <strike>В рекламе по стандартной цене может быть не более 4 прикреплённых изображений. Больше изображений может быть прикреплено за дополнительную плату.</strike></c>
		<c><n>7.4</n> Мы не устанавливаем ограничения на количество символов текста в рекламе.</c>
		<c><n>7.5</n> Максимальное время разового закрепления рекламного поста - 24 часа.</c>
		<c><n>7.6</n> Заведения типа <a href="https://ru.wikipedia.org/wiki/%D0%A2%D1%80%D0%B5%D1%82%D1%8C%D0%B5_%D0%BC%D0%B5%D1%81%D1%82%D0%BE">«третье место»</a> (антикафе, хостелы, коворкинги, клубы по интересам) имеют право на три первых рекламных поста бесплатно.</c>
		<c><n>7.7</n> Рекламные бонусы</c>
				<cc><n>7.7.1</n> <strike>При размещении рекламы на сумму более 2000 руб. в месяц – один дополнительный пост от имени группы бесплатно.</strike></cc>
				<cc><n>7.7.2</n> <strike>Самая лайковая несрочная реклама (см. пункт 0.6) за календарный месяц (январь, февраль и т.д.) – один дополнительный пост от имени группы бесплатно. Победитель определяется в конце месяца.</strike></cc>
		<c><n>7.8</n> Администрация группы и лица, размещающие рекламу от имени группы не несут ответственности за достоверность сведений и результаты деятельности рекламодателя. За достоверность публикуемого материала отвечает рекламодатель либо его доверенное лицо.</c>
		<c><n>7.9</n> При размещении рекламы от имени группы, безвозмездно или с оплатой, рекламодатель не может рассчитывать на смягчение санкций в отношении себя, включая снятие или сокращение срока бана.</c>
		<c><n>7.10</n> Запрещено давать комментарий под постом от имени сообщества вконтакте. исключение составляют комментарии от имени самой Доски Объявлений, а также комментарии от имени группы рекламодателя под постом, содержащим его рекламу.</c>
		<c><n>7.11</n> Услуги, оказываемые бесплатно (клиент не платит за услугу, в том числе компенсацию за расходные материалы, бензин и т.п.) на данный момент не рассматриваются как реклама, и анонсы о данных услугах могут размещаться на стене группы по правилам обычного частного объявления.</c>
		<c><n>7.12</n> Не размещаем ссылки на аккаунты <abbr title="признана в России экстремистской организацией">Instagram<sup>*</sup></abbr>.</c>

		<h2>8. Социальное содействие</h2>

		<c><n>8.1</n> Срочные социально значимые некоммерческие объявления могут быть размещены от имени группы, для этого нужно обратиться к администрации: </c>
		<c><n>8.2</n> Некоммерческие организации (НКО), работающие в Печенгском районе и имеющие центральный офис в Мурманской области, могут размещать свою информацию и анонсы от имени группы бесплатно раз в неделю. Также их пост может быть закреплен в свободное от рекламных закрепов время.</c>
				<cc><n>8.2.1</n> Размещение информации остальных НКО – на усмотрение модераторов.</cc>

		<h2>9. Бонусы</h2>

		<c><n>9.1</n> Нерекламный пост, набравший более 100 лайков может быть повторен от имени группы и закреплен в свободное от запланированных закрепов время. Ссылка на автора – по желанию самого автора.</c>

		</div>
	</div>
</div>


<?= footer() ?>

<?= yandex_counter() ?>

</body>
</html>
