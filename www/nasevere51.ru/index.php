<!DOCTYPE HTML>
<!-- Make NaSevere Great Again! -->
<?php include 'blocks.php' ?>
<html>
<?php $PAGE_ID = "index" ?>
<?= page_header($PAGE_ID) ?>

<body>

<header>

	<div class="container fixed-width">
		<div class="row">
			<div class="col-sm-12">
				<img src="/img/header.png" alt="header.png" width="750" height="100" class="ccm-image-block img-responsive bID-90">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<?= page_navbar($PAGE_ID) ?>
			</div>
		</div>
	</div>
</header>

<div class="container fixed-width" style="margin-top: 20px">
	<div class="row">
		<div class="col-sm-12">

			<h1 class="page-title">Добро пожаловать на сайт группы!</h1>

			<img src="img/strip.jpg" alt="strip.jpg" width="1070" height="210" class="ccm-image-block img-responsive bID-107">

			<hr/>

			<p>С <b>2009</b> года мы развиваем группу <b><a href="http://vk.com/nasevere">Nasevere</a></b> как самую полную и удобную виртуальную доску объявлений для Печенгского района.</p>

			<p>Со временем группа стала не просто доской объявлений, но и местом социализации и обмена разнообразной информацией между жителями Никеля, Заполярного, Печенги, Корзуново и Луостари.</p>

			<p>По количеству участников группа превзошла все остальные группы Печенгского района как вконтакте, так и в одноклассниках (включая даже соседские норвежские группы в фейсбуке). Чем мы очень гордимся, ведь это результат неустанной и регулярной работы команды модераторов. А также продуманной <a href="rules.php">системы правил.</a></p>

			<p>Сейчас количество пользователей группы превысило отметку <b>35000</b>, при среднесуточном охвате в <b>15000-20000</b> человек. Её рост продолжается. Посмотрите статистику и убедитесь в этом сами: <a href="https://vk.com/stats?gid=6126497"><b>Статистика группы</b></a>.</p>

		</div>
	</div>
</div>

<?= footer() ?>

<?= yandex_counter() ?>

</body>
</html>
