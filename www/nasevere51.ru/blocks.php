<?php

include 'globals.php';

function page_title($page_id) {
  global $PAGES;
  return $PAGES[$page_id]["title"];
}

function page_header($page_id) {
  $strf = <<<HDR
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>Доска объявлений Печенгского района :: %s</title>
    <meta name="description" content="" />
  	<link href="css/nasevere51.ru.css" rel="stylesheet" type="text/css" media="all">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0,  maximum-scale=1.0, user-scalable=no">
  </head>
HDR;
  return sprintf($strf, page_title($page_id));
}

function page_navbar($page_id) {
  global $PAGES;
  $navs = "";
  foreach ($PAGES as $k => $v) {
    $navs .= sprintf('<div class="menuitem "><a href="%s" target="_self" class="%s">%s</a></div>',
                      $v['url'],
                      ($k == $page_id)? "nav-selected nav-path-selected" : "",
                      $v['title']);
  }
  $strf = <<<NAV
  <div style="background: #333">
    <div class="navbar">
      %s
    </div>
  </div>
NAV;
  return sprintf($strf, $navs);
}

function page_bottom_bar() {
  return <<<BNAV
  <div class="row">
    <div class="col-sm-12">
      <div style="height: 20px;"></div>
      <div style="background: #4C75A3">
        <div class="navbar">
          <div class="menuitem "><a href="http://old.nasevere51.ru/useful/transport" target="_self" class="">Транспорт</a></div>
          <div class="menuitem "><a href="https://vk.com/wall-6126497?search=1" target="_blank" class="">Поиск в группе</a></div>
          <div class="menuitem "><a href="http://run5.ru" target="_blank" class="">Где пробежаться</a></div>
        </div>
      </div>
    </div>
  </div>
BNAV;
}

function yandex_counter() {
  return
  <<<YC
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript">
  	(function (d, w, c) {
  		(w[c] = w[c] || []).push(function() {
  			try {
  				w.yaCounter33159448 = new Ya.Metrika({
  					id:33159448,
  					clickmap:true,
  					trackLinks:true,
  					accurateTrackBounce:true
  				});
  			} catch(e) { }
  		});

  		var n = d.getElementsByTagName("script")[0],
  						s = d.createElement("script"),
  						f = function () { n.parentNode.insertBefore(s, n); };
  		s.type = "text/javascript";
  		s.async = true;
  		s.src = "https://mc.yandex.ru/metrika/watch.js";

  		if (w.opera == "[object Opera]") {
  			d.addEventListener("DOMContentLoaded", f, false);
  		} else { f(); }
  	})(document, window, "yandex_metrika_callbacks");
  </script>
  <noscript><div><img src="https://mc.yandex.ru/watch/33159448" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->
YC;
}

function footer() {
  return
  <<<FTR
  <footer id="site-footer">
  	<div class="container fixed-width">
  		<div class="row">
  			<div
  							class="col-sm-12">2015&ndash;2020, Группа НаСевере:
  				<a href="http://vk.com/nasevere">vk.com/nasevere</a></div>
  		</div>
  	</div>
  </footer>
FTR;
}

?>
