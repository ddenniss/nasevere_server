#lang racket

(require odysseus)
(require tabtree)
(require tabtree/template-functions)
(require tabtree/html)
(require vk)
(require odysseus/csv)
(require dataviz/barchart)
(require dataviz/svg-light)
(require (file "~/settings/private_settings/APIs.rkt"))
(require "../_lib/template-functions.rkt")
(require "../_lib/functions.rkt")
(require "../_lib/wall.rkt")
(require "../_lib/globals.rkt")
(require "../_lib/init.rkt")
(require "../_lib/clients.rkt")

(provide (all-defined-out))

(define-namespace-anchor a)
(define ns (namespace-anchor->namespace a))

(define reported-month MONTH)
(define reported-year YEAR)

(define pub-summary-h (make-parameter (hash)))
(define gid ($ nasevere vk/groups))
(define items (let* (
                            (offset0 (* 0 3 30 avg-daily-group-posts))
                            (items (read-serialized-data-from-file GROUP_POSTS_CACHE))
                            (days30 (* 60 60 24 30))
                            (items (filter (λ (x)
                                            (and
                                              (d>= (seconds->datestr ($ date x)) (format "01.~a.~a" reported-month reported-year))
                                              (d<= (seconds->datestr ($ date x)) (format "~a.~a.~a" MONTH_LAST_DAY reported-month reported-year))))
                                          items))
                            (items (sort items (λ (a b) (< ($ date a) ($ date b))))))
                          items))
(Items-this-month items)

(define processed-items (let ((i 0))
                                  (for/fold
                                    ((res empty))
                                    ((item items))
                                    (let* ((moderator (detect-ad-moderator item))
                                          (prime-moderator (last (string-split moderator ",")))
                                          (count (hash-ref (pub-summary-h) prime-moderator 0))
                                          (count (inc count))
                                          (_ (when moderator (pub-summary-h (hash-set (pub-summary-h) prime-moderator count)))))
                                      (set! i (+ i 1))
                                      (pushr res
                                        (hash
                                          'index i
                                          'title (string-take (string-replace ($ text item) "\t" "") 80)
                                          'url (format "https://vk.com/wall-~a_~a" gid ($ __id item))
                                          'date (hdate->string (seconds->hdate ($ date item)))
                                          'img-url (get-img-url item)
                                          'likes ($ likes.count item)
                                          'text ($ text item)
                                          'mod moderator
                                          'prime-mod prime-moderator
                                          'moderator-class (if moderator "moderator" "no-moderator")
                                          'moderator-fullname (actual-moderator-fullname moderator #:unknown-default "нерекламный пост")
                                          ))))))

(for/fold
  ((res ""))
  ((p processed-items))
  (begin
    (--- ($ moderator-class p) ($ moderator-fullname p))
    #t))


; (define publications-count-html (let* ((count-cons (map
;                                                             (λ (x) (cons x (hash-ref (pub-summary-h) x)))
;                                                             (hash-keys (pub-summary-h))))
;                                             (count-cons (sort count-cons (λ (a b) (> (cdr a) (cdr b))))))
;                                         (for/fold
;                                           ((res ""))
;                                           ((person count-cons))
;                                           (if-not person
;                                             res
;                                             (str
;                                               res
;                                               (format "<tr><td><b class=\"moderator\">~a</b></td><td><div class=\"bar\" style=\"width:~apx\">~a</div></td></tr>"
;                                                       (actual-moderator-fullname (car person))
;                                                       (+ 10 (* 4 (->number (cdr person))))
;                                                       (cdr person)))))))
