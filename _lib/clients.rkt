#lang racket

(require odysseus)
(require tabtree)
(require odysseus/csv)
(require odysseus/time)
(require odysseus/text)
(require vk)
(require "wall.rkt")
(require "init.rkt")
(require "good-old-odysseus.rkt")

(provide (all-defined-out))

; (hash 151752177 <client-1> "СУШИ WOK" <client-2> ...)
(define-catch (build-trigger-client-table clients galias-gid)
  (if (not clients)
      (hash)
      (hash
          'vkname:clientid
              (for/fold
                ((res (hash)))
                (((id client) clients))
                (let* (
                      (vk-aliases (or ($ vk client) empty))
                      (vk-aliases (if (list? vk-aliases) vk-aliases (list vk-aliases)))
                      (vk-aliases (map get-alias vk-aliases))
                      (vk-ids (cleanmap (map (λ (vk-alias) (hash-ref galias-gid vk-alias #f)) vk-aliases)))
                      (vk-all-names (append vk-ids vk-aliases)))
                  ; (when (equal? id "Пых_пых")
                  ;   (--- vk-aliases "--" vk-ids "--" vk-all-names))
                  (cond
                    ((empty? vk-all-names) res)
                    (else (hash-union
                            (for/hash
                              ((vk-name vk-all-names))
                              (values vk-name id))
                            res)))))
          'keyword:clientid
              (for/fold
                ((res (hash)))
                (((id client) clients))
                (hash-union
                  res
                  (for/hash
                    ((keyword (get-values (get-keywords client) "//")))
                    (values keyword id))))
          'exact-keyword:clientid
              (for/fold
                ((res (hash)))
                (((id client) clients))
                (hash-union
                  res
                  (for/hash
                    ((exact-keyword (get-values (get-exact-keywords client) "//")))
                    (values exact-keyword id))))
          'phone:clientid
              (for/fold
                ((res (hash)))
                (((id client) clients))
                (hash-union
                  res
                  (for/hash
                    ((phone (get-values ($ phone client))))
                    (values phone id))))
          'site:clientid
              (for/fold
                ((res (hash)))
                (((id client) clients))
                (hash-union
                  res
                  (for/hash
                    ((url (append
                                (get-values ($ url client))
                                (get-values ($ email client))
                                (get-values ($ app-url client)))))
                    (values url id)))))))


(define TRIGGER-TAB (make-parameter (build-trigger-client-table CLIENTS GALIAS_GID)))
; (define TRIGGER-TAB (make-parameter (hash)))

(define-catch (get-moderator-name-by-abbr abbr default)
  (let* (
        (abbr (if (list? abbr) (last abbr) abbr))
        (targeted-moderator (filter
                              (λ (item) (let ((abbrs (hash-ref item "abbr" NONE)))
                                          (if (list? abbrs)
                                            (index-of? ($ abbr item) abbr)
                                            (equal? abbrs abbr))))
                              admins))
        (result (if-not (empty? targeted-moderator)
                        ($ __id (last targeted-moderator))
                        default)))
      result))

(define-catch (moderators-fullnames abbr #:unknown-default (unknown-default "?"))
  (implode
    (map
      (λ (x) (namefy (get-moderator-name-by-abbr x unknown-default)))
      (listify abbr))
    ", "))

(define-catch (actual-moderator-fullname abbr #:unknown-default (unknown-default "?"))
  (namefy (get-moderator-name-by-abbr abbr unknown-default)))

(define-catch (make-time-order clients-tabtree)
  (let ((clients-new-order
          (sort clients-tabtree
            (λ (a b)
              (let ((last-a ($ last a))
                    (last-b ($ last b)))
              (and last-a last-b (d> last-a last-b)))))))
    (for/fold
      ((res empty))
      ((client clients-new-order) (i (in-naturals)))
      (pushr
        res
        (hash-union
          (hash 'time-order (inc i))
          client)))))

(define (client->moderator item)
  (let* ((mod ($ mod item))
        (mod (and mod (car (string-split mod ",")))))
    mod))

(define-catch (detect-ad-client item)
  (define (normalize-phone-number phone)
    ; ((change-text '((" " . "") ("+7" . "8") ("-" . "") ("(" . "") (")" . ""))) phone))
    (-> phone
        (string-replace #rx"[ +()\\-]" "")
        (string-replace #rx"^7" "8")))
  (let* (
        (text (get-post-text item))
        (initial-text text)
        (text (if (list? text) (first text) text))
        (triggers-table (TRIGGER-TAB))
        (vkname:clientid ($ vkname:clientid triggers-table))
        (phone:clientid ($ phone:clientid triggers-table))
        (keyword:clientid ($ keyword:clientid triggers-table))
        (exact-keyword:clientid ($ exact-keyword:clientid triggers-table))
        (site:clientid ($ site:clientid triggers-table))
        (group-or-user-id (get-matches #px"(club|id|public)(\\d+)" text))
        (group-or-user-id (and (not-empty? group-or-user-id) (not-empty? (first group-or-user-id)) (third (first group-or-user-id))))
        (group-or-user-names (get-matches #px"vk\\.com/([A-Za-z_0-9.]+[A-Za-z0-9_])" text))
        (group-or-user-names (map (λ (x) (second x)) group-or-user-names))
        (repost-group-id (get-repost-group-id item))
        (client-ids (cleanmap (append (list group-or-user-id) group-or-user-names (list repost-group-id))))
        (client-by-vk-id (ormap (λ (x) (and x (hash-ref* vkname:clientid x))) client-ids))
        (text-to-detect-phone (normalize-phone-number text))
        (phone-matching-regexp #px"(\\d{11})(?=([^0-9]|$))")
        (phones (get-matches phone-matching-regexp text-to-detect-phone))
        ; (_ (when (equal? client-ids '(188302505)) (--- client-by-vk-id vkname:clientid)))
        (phones (filter-not
                  (λ (x) (equal? "" x))
                  (map (λ (x) (second x)) phones)))
        (client-by-phone (for/fold
                                  ((res #f))
                                  ((phone (map normalize-phone-number phones)))
                                  (or res (hash-ref* phone:clientid phone))))
        ; (_ (when (string-contains? initial-text "79211792552")
        ;   (--- client-by-vk-id client-by-phone phones client-ids)))
        (sites (cleanmap (get-matches #px"(www\\.|https?://)(.*?\\.(ru|su|com|org|рф|pro|cc))" text)))
        (sites (filter-not
                  (λ (x) (equal? "" x))
                  (map (λ (x) (third x)) sites)))
        (client-by-site (for/fold
                                  ((res #f))
                                  ((site sites))
                                  (or res (hash-ref* site:clientid site))))
        (keywords (filter (λ (x) (and x (re-matches? (pregexp (string-downcase x)) (string-downcase text)))) (hash-keys keyword:clientid)))
        (client-by-keyword (for/fold
                                  ((res #f))
                                  ((keyword keywords))
                                  (or res (hash-ref* keyword:clientid keyword))))
        (exact-keywords (filter (λ (x) (and x (string-contains? text x))) (hash-keys exact-keyword:clientid)))
        (client-by-exact-keyword (for/fold
                                  ((res #f))
                                  ((exact-keyword exact-keywords))
                                  (or res (hash-ref* exact-keyword:clientid exact-keyword))))
        ; (_ (when (string-contains? initial-text "https://vk.com/avtonazakaz51")
        ; (_ (when (string-contains? text "8(81554) 6-01-23")
        ;   (--- ($ id item) client-by-vk-id client-by-phone client-by-site client-by-exact-keyword client-by-keyword)))
        (clientid (or client-by-vk-id client-by-phone client-by-site client-by-exact-keyword client-by-keyword))
        (client (and clientid (hash-ref* CLIENTS clientid)))
        (hdate (seconds->hdate ($ date item)))
        ; (_ (when (and (= ($ hour hdate) 19) (= ($ min hdate) 31) (= ($ day hdate) 28))
        ;       (--- client-by-vk-id client-by-phone client-by-site client-by-exact-keyword client-by-keyword)))
        )

    ; (188302505) #hash((__id . Армейский) (__line . 56) (__parent . старые_клиенты) (cat . (Магазин_одежды Магазин_обуви)) (last . 14.08.2018) (mod . dd) (phone . 89210463434) (service . магазин армейской одежды и обуви) (vk . vk.com/club125131796)) dd dd

    ; (when (equal? ($ mod client) "dd")
    ;   (--- client))

    client))

(define-catch (detect-ad-moderator item)
  (let ((client (detect-ad-client item)))
    (and client ($ mod client))))

(define-catch (detect-client-id item)
  (let ((client (detect-ad-client item)))
    (and client ($ __id client))))

(define-catch (old-client? client)
  (has-parent? client "старые_клиенты"))
