CMD_DIR = _cmd

wall_posts:
	cd $(CMD_DIR); racket load-group-posts.rkt

pinups:
	cd $(CMD_DIR); racket load-pinups.rkt

stats:
	cd $(CMD_DIR); racket update-stats.rkt

invoices:
	cd $(CMD_DIR); racket make-invoices.rkt

update_clients:
	cd $(CMD_DIR); racket cache-vk-ids.rkt
